
package com.jumbotail.weather.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Forecast {

    @SerializedName("forecast")
    @Expose
    private Forecast_ forecast;

    /**
     * 
     * @return
     *     The forecast
     */
    public Forecast_ getForecast() {
        return forecast;
    }

    /**
     * 
     * @param forecast
     *     The forecast
     */
    public void setForecast(Forecast_ forecast) {
        this.forecast = forecast;
    }

}
