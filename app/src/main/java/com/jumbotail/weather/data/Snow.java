
package com.jumbotail.weather.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Snow {

    @SerializedName("in")
    @Expose
    private Integer in;
    @SerializedName("cm")
    @Expose
    private Integer cm;

    /**
     * 
     * @return
     *     The in
     */
    public Integer getIn() {
        return in;
    }

    /**
     * 
     * @param in
     *     The in
     */
    public void setIn(Integer in) {
        this.in = in;
    }

    /**
     * 
     * @return
     *     The cm
     */
    public Integer getCm() {
        return cm;
    }

    /**
     * 
     * @param cm
     *     The cm
     */
    public void setCm(Integer cm) {
        this.cm = cm;
    }

}
