package com.jumbotail.weather.card;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jumbotail.weather.R;
import com.jumbotail.weather.data.CurrentObservation;
import com.jumbotail.weather.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by shadmananwer on 25/01/16.
 */
public class DetailsCard extends Card {
    private ImageView mWeatherImg;
    private TextView mDetailsText;
    private LinearLayout mDetailsLinearLayout;

    public DetailsCard(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.details_layout;
    }

    @Override
    protected void configureUi() {
        mWeatherImg = (ImageView) findViewById(R.id.weather_image);
        mDetailsText = (TextView) findViewById(R.id.details_text);
        mDetailsLinearLayout = (LinearLayout) findViewById(R.id.details_list);
    }

    @Override
    protected void configureHandleEvents() {

    }

    public void setData(CurrentObservation observation) {
        int dp_56 = (int) Utils.convertDpToPixel(56, getContext());

        String details = "";
        if ( !Utils.IsEmpty(observation.getWindDir())
                && !Utils.IsEmpty(observation.getWindKph().toString()) ) {
            details = "Winds from " + observation.getWindDir() + " at " + observation.getWindKph() + " kmph";
        } else if ( !Utils.IsEmpty(observation.getWindDir())
                && Utils.IsEmpty(observation.getWindKph().toString()) ){
            details = "Winds from " + observation.getWindDir();
        } else if ( Utils.IsEmpty(observation.getWindDir())
                && !Utils.IsEmpty(observation.getWindKph().toString()) ) {
            details = "Winds at " + observation.getWindKph() + " kmph";
        }

        mDetailsText.setText(details);

        ArrayList<DetailsPair> pairList = new ArrayList<>();
        pairList.add(new DetailsPair("Feels like", observation.getFeelslikeC() + " \u2103"));
        pairList.add(new DetailsPair("Humidity", observation.getRelativeHumidity()));
        pairList.add(new DetailsPair("Visibility", observation.getVisibilityKm() + " km"));
        pairList.add(new DetailsPair("UV Index", observation.getUV()));
        pairList.add(new DetailsPair("Heat index", observation.getHeatIndexC()));
        pairList.add(new DetailsPair("Precipitation", observation.getPrecipTodayMetric()));

        int i = 0;
        mDetailsLinearLayout.removeAllViews();
        for ( DetailsPair pair : pairList ) {
            DetailsItem item = new DetailsItem(getContext());
            mDetailsLinearLayout.addView(item.createCardView(mDetailsLinearLayout, false));
            item.setData(pair.key, pair.val);

            i++;
            if ( i % 2 == 0 )
                item.toneColor(true);
        }
    }

    private static class DetailsPair {
        public DetailsPair(String key, String val) {
            this.key = key;
            this.val = val;
        }

        String key;
        String val;
    }
}
