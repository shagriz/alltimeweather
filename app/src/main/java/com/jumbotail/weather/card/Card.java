package com.jumbotail.weather.card;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by shadmananwer on 25/01/16.
 */
public abstract class Card {

    private Context mContext;
    private View mRootView;

    public Card(Context context) {
        mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

    public View createCardView(ViewGroup parent, boolean attach) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mRootView = inflater.inflate(getLayoutResource(), parent, attach);

        configureUi();
        configureHandleEvents();

        return mRootView;
    }

    protected View getRootView() {
        return mRootView;
    }

    protected View findViewById(int id) {
        return mRootView.findViewById(id);
    }

    protected abstract int getLayoutResource();
    protected abstract void configureUi();
    protected abstract void configureHandleEvents();
}
