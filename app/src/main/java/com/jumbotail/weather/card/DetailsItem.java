package com.jumbotail.weather.card;

import android.content.Context;
import android.widget.TextView;

import com.jumbotail.weather.R;

/**
 * Created by shadmananwer on 25/01/16.
 */
public class DetailsItem extends Card {
    private TextView mDetailsField;
    private TextView mDetailsValue;

    public DetailsItem(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.details_item;
    }

    @Override
    protected void configureUi() {
        mDetailsField = (TextView) findViewById(R.id.details_field);
        mDetailsValue = (TextView) findViewById(R.id.details_value);
    }

    @Override
    protected void configureHandleEvents() {

    }

    public void setData(String filed, String value) {
        mDetailsField.setText(filed);
        mDetailsValue.setText(value);
    }

    public void toneColor(boolean tone) {
        if ( tone ) {
            getRootView().setBackgroundColor(getContext().getResources().getColor(R.color.background));
        } else {
            getRootView().setBackgroundColor(0);
        }
    }
}
