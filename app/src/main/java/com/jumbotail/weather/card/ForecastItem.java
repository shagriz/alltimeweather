package com.jumbotail.weather.card;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jumbotail.weather.R;
import com.jumbotail.weather.data.Forecastday;
import com.jumbotail.weather.utils.Utils;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by shadmananwer on 25/01/16.
 */
public class ForecastItem extends Card {
    private TextView mDayText;
    private ImageView mWeatherImg;
    private TextView maxTemp;
    private TextView minTemp;
    private TextView weatherText;
    private TextView windText;

    public ForecastItem(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.forecast_item;
    }

    @Override
    protected void configureUi() {
        mDayText = (TextView) findViewById(R.id.forecast_day);
        mWeatherImg = (ImageView) findViewById(R.id.forecast_image);
        minTemp = (TextView) findViewById(R.id.min_temp);
        maxTemp = (TextView) findViewById(R.id.max_temp);
        weatherText = (TextView) findViewById(R.id.forecast_weather);
        windText = (TextView) findViewById(R.id.forecast_wind);
        windText.setVisibility(View.GONE);
    }

    @Override
    protected void configureHandleEvents() {

    }

    public void setData(Forecastday forecastday) {
        Date date = new Date(Long.parseLong(forecastday.getDate().getEpoch()) * 1000);
        mDayText.setText((new SimpleDateFormat("cccc")).format(date));
        minTemp.setText(forecastday.getLow().getCelsius() + " \u2103");
        maxTemp.setText(forecastday.getHigh().getCelsius() + " \u2103");
        weatherText.setText(Utils.ToTitleCase(forecastday.getConditions()));
        windText.setText("Wind " + forecastday.getAvewind().getKph() + " kmph");

        int dp_24 = (int) Utils.convertDpToPixel(24, getContext());
        Picasso.with(getContext())
                .load(forecastday.getIconUrl())
                .centerCrop()
                .resize(dp_24, dp_24)
                .into(mWeatherImg);
    }
}
