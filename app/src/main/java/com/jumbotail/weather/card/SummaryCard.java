package com.jumbotail.weather.card;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.jumbotail.weather.R;
import com.jumbotail.weather.data.CurrentObservation;
import com.jumbotail.weather.utils.Utils;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by shadmananwer on 25/01/16.
 */
public class SummaryCard extends Card {
    private TextView mPlaceName;
    private TextView mTimeText;
    private TextView mWeatherText;
    private TextView mTempText;
    private ImageView mWeatherImg;

    public SummaryCard(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.summary_card;
    }

    @Override
    protected void configureUi() {
        mPlaceName = (TextView) findViewById(R.id.place_name);
        mTimeText = (TextView) findViewById(R.id.date_time);
        mWeatherText = (TextView) findViewById(R.id.weather_now);
        mTempText = (TextView) findViewById(R.id.weather_degree);
        mWeatherImg = (ImageView) findViewById(R.id.weather_image);

        TextView degreeText = (TextView) findViewById(R.id.weather_degree_symbol);
        degreeText.setText("\u2103");
    }

    @Override
    protected void configureHandleEvents() {

    }

    public void setData(CurrentObservation observation) {
        mPlaceName.setText(observation.getDisplayLocation().getFull());
        mTimeText.setText((new SimpleDateFormat("cccc hh:mm a")).format(new Date()));
        mWeatherText.setText(Utils.ToTitleCase(observation.getIcon()));
        mTempText.setText(String.valueOf(observation.getTempC()));

        int dp_56 = (int) Utils.convertDpToPixel(56, getContext());

        Picasso.with(getContext())
                .load(observation.getIconUrl())
                .centerCrop()
                .resize(dp_56, dp_56)
                .into(mWeatherImg);
    }
}
