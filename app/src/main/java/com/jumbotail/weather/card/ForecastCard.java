package com.jumbotail.weather.card;

import android.content.Context;
import android.widget.LinearLayout;

import com.jumbotail.weather.R;
import com.jumbotail.weather.data.Forecast_;
import com.jumbotail.weather.data.Forecastday;

import java.util.ArrayList;

/**
 * Created by shadmananwer on 25/01/16.
 */
public class ForecastCard extends Card {
    private LinearLayout mForecastList;

    public ForecastCard(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.forecast_layout;
    }

    @Override
    protected void configureUi() {
        mForecastList = (LinearLayout) findViewById(R.id.forecast_details);
    }

    @Override
    protected void configureHandleEvents() {

    }

    public void setData(Forecast_ forecast) {
        mForecastList.removeAllViews();
        for ( Forecastday forecastday : forecast.getSimpleforecast().getForecastday()) {
            ForecastItem item = new ForecastItem(getContext());
            mForecastList.addView(item.createCardView(mForecastList, false));
            item.setData(forecastday);
        }
    }
}
