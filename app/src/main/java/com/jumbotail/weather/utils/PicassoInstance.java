package com.jumbotail.weather.utils;

import android.content.Context;
import android.util.Log;

import com.jumbotail.weather.BuildConfig;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class PicassoInstance {
    private static Picasso myPicassoInstance = null;
    private static final long CACHE_SIZE = 1024 * 1024 * 50;

    public static void setPicassoSingleton(Context context) {
        if (myPicassoInstance == null) {
            myPicassoInstance = createMyPicassoInstance(context);
            Picasso.setSingletonInstance(myPicassoInstance);
            if ( BuildConfig.DEBUG) {
                Log.i("PICASSO INSTANCE", "CREATED");
            }
        }
    }

    private static Picasso createMyPicassoInstance(Context context) {
        File cacheDir = new File(context.getCacheDir().getPath() + File.separator + "TLImageCache");
        if ( !cacheDir.exists() ) {
            cacheDir.mkdir();
        }
        Cache cache = new Cache(cacheDir, CACHE_SIZE);

        OkHttpClient myOkHttpClient = new OkHttpClient();
        myOkHttpClient.setConnectTimeout(20, TimeUnit.SECONDS);
        myOkHttpClient.setReadTimeout(20, TimeUnit.SECONDS);
        myOkHttpClient.setCache(cache);
        myOkHttpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Cookie", "triplived").build();
                if (BuildConfig.DEBUG) {
                    Log.i("ON INTERCEPT", "COOKIE ADDED");
                    Log.i("ON INTERCEPT", newRequest.url().toString());
                }

                Response response = chain.proceed(newRequest);
                return response.newBuilder()
                        .header("Cache-Control", "max-age=" + (60 * 60 * 24 * 365))
                        .build();
            }
        });

        if ( BuildConfig.DEBUG ) {
            return new Picasso.Builder(context).downloader(
                    new OkHttpDownloader(myOkHttpClient))
                    .build();
        } else {
            return new Picasso.Builder(context).downloader(
                    new OkHttpDownloader(myOkHttpClient))
                    .build();
        }
    }
}