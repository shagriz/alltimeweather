package com.jumbotail.weather.utils;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;

/**
 * Created by shadmananwer on 26/01/16.
 */
public class LocationUtils {

    private static LocationUtils sLocationUtils;
    private LocationManager mLocationManager;

    private LocationUtils() {
        mLocationManager = (LocationManager) Utils.GetApplicationContext()
                .getSystemService(Context.LOCATION_SERVICE);
    }

    private static LocationUtils getInstance() {
        if ( sLocationUtils == null ) {
            sLocationUtils = new LocationUtils();
        }

        return sLocationUtils;
    }

    public static Location GetLastKnowLocationCoarse() {
        String provider = LocationManager.NETWORK_PROVIDER;
        return getInstance().mLocationManager.getLastKnownLocation(provider);
    }

    public static void RequestLocationCoarse(LocationCallback callback) {
        String provider = LocationManager.NETWORK_PROVIDER;
        getInstance().mLocationManager.requestLocationUpdates(provider, 1000, 0.5f,
                new Listener(callback), Looper.getMainLooper() );
    }

    private static class Listener implements LocationListener {
        private LocationCallback callback;

        public Listener(LocationCallback callback) {
            this.callback = callback;
        }

        @Override
        public void onLocationChanged(Location location) {
            getInstance().mLocationManager.removeUpdates(this);
            if ( callback != null ) {
                callback.onLocationReceived(location);
            }

            // Set callback to null to avoid multiple callback calls.
            callback = null;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }

    public interface LocationCallback {
        void onLocationReceived(Location location);
    }
}
