package com.jumbotail.weather.utils;

import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.jumbotail.weather.BuildConfig;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

import static com.squareup.okhttp.internal.Util.UTF_8;

/**
 * Created by shadmananwer on 25/01/16.
 */
public class NetworkUtils {
    private static final String TAG = "network_utils";

    private static String API_KEY = "205659fbe9e98fdb";
    private static String WEATHER_API_STRING = "http://api.wunderground.com/api/%s/conditions/q/%s.json";
    private static String FORECAST_API_STRING = "http://api.wunderground.com/api/%s/forecast10day/q/%s.json";

    private static OkHttpClient gOkHttpClient = null;

    /**
     * Returns the singleton OkHttpClient for network access.
     * @return
     */
    public static OkHttpClient GetHttpClient() {
        if ( gOkHttpClient == null ) {
            gOkHttpClient = new OkHttpClient();

            if ( BuildConfig.DEBUG ) {
                gOkHttpClient.setConnectTimeout(20, TimeUnit.SECONDS);
                gOkHttpClient.setReadTimeout(20, TimeUnit.SECONDS);
            }

            try {
                int cache_size = 10 * 1024 * 1024; // 10 MiB
                File cacheFile = new File(Utils.GetApplicationContext().getCacheDir(), "okHttpCache");
                Cache cache = new Cache(cacheFile, cache_size);
                gOkHttpClient.setCache(cache);
            } catch ( Exception e ) {
                e.printStackTrace();
                Log.e(TAG, e.getMessage() + " at line " + 28);
            }
        }

        return gOkHttpClient;
    }

    public static String GetWeatherURL(Location location) {
        String strLoc = location.getLatitude() + "," + location.getLongitude();
        return GetWeatherURL(strLoc);
    }

    public static String GetWeatherURL(String place) {
        return String.format(WEATHER_API_STRING, API_KEY, place);
    }

    /**
     * Get the forecast URL String string generated for the given place.
     *
     * @param location
     * @return
     */
    public static String GetForecastURL(Location location) {
        String strLoc = location.getLatitude() + "," + location.getLongitude();
        return GetForecastURL(strLoc);
    }

    /**
     * Get the forecast URL String string generated for the given place.
     *
     * @param place
     * @return
     */
    public static String GetForecastURL(String place) {
        return String.format(FORECAST_API_STRING, API_KEY, place);
    }

    /**
     * Send a synchronous GET request to the provided URL.
     *
     * @param url
     * @return
     * @throws IOException
     */
    public static CompoundResponse SendSyncRequest(String url) throws IOException {
        return processUrl(url, null, null);
    }

    /**
     * Send an asynchronous GET request to the provided URL.
     *
     * @param url
     * @param progressCallback
     * @param callback
     */
    public static void SendAsyncRequest(String url, ProgressCallback progressCallback, CompleteCallback callback) {
        Looper looper = Looper.myLooper();
        Handler handler = null;
        if ( looper != null )
            handler = new Handler(looper);

        AsyncProcessThread thread = new AsyncProcessThread(url, handler, progressCallback, callback);
        thread.start();
    }

    /**
     * Check for the internet availability.
     *
     * @return true if internet connection is available.
     */
    public static boolean IsNetworkAvailable() {
        boolean isNetworkAvailable = false;
        ConnectivityManager connMgr = (ConnectivityManager) Utils.GetApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null) {
            isNetworkAvailable = networkInfo.isConnected();
            if (isNetworkAvailable) {
                Log.i(TAG, "Network Type: " + networkInfo.getTypeName());
            } else {
                Log.i(TAG, "Network State Reason: " + networkInfo.getReason());
            }
        }

        Log.i(TAG, "Network Status: " + isNetworkAvailable);
        return isNetworkAvailable;
    }

    private static Charset charset(ResponseBody body) {
        MediaType contentType = body.contentType();
        return contentType != null ? contentType.charset(UTF_8) : UTF_8;
    }

    private static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException rethrown) {
                throw rethrown;
            } catch (Exception ignored) {
            }
        }
    }

    private static CompoundResponse processUrl(String url, Handler handler, final ProgressCallback callback)
            throws IOException {
        Log.d(TAG, "Sending request to url - " + url);
        Request request = new Request.Builder().url(url).build();
        Call call = GetHttpClient().newCall(request);

        Response response = call.execute();
        InputStream inputStream = response.body().byteStream();

        byte[] buff = new byte[1024];
        String output = "";
        long downloaded = 0;
        final long target = response.body().contentLength();

        if ( handler != null && callback != null ) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    callback.onProgressUpdate(0L, target);
                }
            });
        }

        while ( true ) {
            int read = inputStream.read(buff);
            if ( read == -1 ) {
                break;
            }

            output += new String(buff, 0, read, charset(response.body()).name());
            downloaded += read;

            if ( handler != null && callback != null ) {
                final long finalDownloaded = downloaded;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onProgressUpdate(finalDownloaded, target);
                    }
                });
            }
        }

        closeQuietly(response.body());
        inputStream.close();
        return new CompoundResponse(response, output);
    }

    private static class AsyncProcessThread extends Thread {
        private final String url;
        private final Handler handler;
        private final ProgressCallback progressCallback;
        private final CompleteCallback callback;

        private AsyncProcessThread(String url, Handler handler,
                                   ProgressCallback progressCallback, CompleteCallback callback) {
            this.url = url;
            this.handler = handler;
            this.progressCallback = progressCallback;
            this.callback = callback;
        }

        @Override
        public void run() {
            try {
                final CompoundResponse response = processUrl(url, handler, progressCallback);
                if ( callback != null && handler != null ) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onComplete(response);
                        }
                    });
                }
            } catch ( final IOException e ) {
                e.printStackTrace();
                if ( callback != null && handler != null ) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onFailure(e);
                        }
                    });
                }
            }
        }
    }

    /**
     * Callback for download progress updates.
     */
    public interface ProgressCallback {
        void onProgressUpdate(long progress, long total);
    }

    /**
     * Callback for request complete/failure
     */
    public interface CompleteCallback {
        void onComplete(CompoundResponse response);
        void onFailure(IOException exception);
    }

    /**
     * Response object with
     */
    public static class CompoundResponse {
        public Response response;
        public String body;

        public CompoundResponse(Response response, String body) {
            this.response = response;
            this.body = body;
        }
    }
}
