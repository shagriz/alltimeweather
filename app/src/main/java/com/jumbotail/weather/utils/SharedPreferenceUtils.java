package com.jumbotail.weather.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by shadmananwer on 25/01/16.
 */
public class SharedPreferenceUtils {
    private static SharedPreferenceUtils sInstance;
    private SharedPreferences mSharedPreferences;

    private static final String SHARED_PREFS_FILE = "weather_preferences";

    public static String KEY_DETAILS_DATA = "key_details_data";
    public static String KEY_FORECAST_DATA = "key_forecast_data";

    public static String KEY_DETAILS_DATE = "key_details_date";
    public static String KEY_FORECAST_DATE = "key_forecast_date";

    private SharedPreferenceUtils() {
        Context context = Utils.GetApplicationContext();
        mSharedPreferences = context.getSharedPreferences(SHARED_PREFS_FILE, Context.MODE_PRIVATE);
    }

    private static synchronized SharedPreferenceUtils getInstance() {
        if (sInstance == null) {
            sInstance = new SharedPreferenceUtils();
        }
        return sInstance;
    }

    public static boolean PutString(String key, String value) {
        SharedPreferences.Editor editor = getInstance().mSharedPreferences.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public static String getString(String key) {
        return getInstance().mSharedPreferences.getString(key, null);
    }

    public static boolean PutLong(String key, long value) {
        SharedPreferences.Editor editor = getInstance().mSharedPreferences.edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    public static long getLong(String key) {
        return getInstance().mSharedPreferences.getLong(key, 0);
    }

}
