package com.jumbotail.weather.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;

/**
 * Created by shadmananwer on 25/01/16.
 */
public class Utils {

    private static final String TAG = "utils";
    private static Context mContext;
    /**
     * This function is used to set application mContext from MMTApplication class.
     * It is called during onCreate of application.
     *
     * @param context
     */
    public static void SetApplicationContext(Context context) {
        mContext = context;
    }

    /**
     * The version number of this package, as specified by the manifest tag's
     * attribute.
     */
    public static Context GetApplicationContext() {
        if ( mContext == null ) {
            throw new RuntimeException("Utils calss not initialized");
        }

        return mContext;
    }

    public static float convertDpToPixel(float dp, Context context) {
        if ( context == null ) {
            Log.e(TAG, "Context is null");
            return 0;
        }
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static float convertPixelToDp(float px, Context context) {
        if ( context == null ) {
            Log.e(TAG, "Context is null");
            return 0;
        }
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static String CopyByteToString(byte[] buff, int start, int end) {
        String string = "";
        for ( int i = 0; i < end; i++ ) {
            string += String.valueOf((char)buff[0]);
        }

        return string;
    }

    public static String ToTitleCase(String input) {
        StringBuilder titleCase = new StringBuilder();
        boolean nextTitleCase = true;

        for (char c : input.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                nextTitleCase = true;
            } else if (nextTitleCase) {
                c = Character.toTitleCase(c);
                nextTitleCase = false;
            }

            titleCase.append(c);
        }

        return titleCase.toString();
    }

    public static boolean IsEmpty(String string) {
        if ( string == null || string.trim().equals("") )
            return true;
        return false;
    }

    public static boolean IsLocationServicesEnabled() {
        boolean network_enabled = false, gps_enabled = false;
        LocationManager locManager = (LocationManager) GetApplicationContext()
                .getSystemService(Context.LOCATION_SERVICE);

        try {
            gps_enabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {}

        return network_enabled;
    }

    public static AlertDialog ShowAlertDialog(Context context, String title, String message,
                                       String affirm, DialogInterface.OnClickListener affirmListener,
                                       String neg, DialogInterface.OnClickListener negListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message);

        if ( affirm != null ) {
            builder.setPositiveButton(affirm, affirmListener);
        }

        if ( neg != null ) {
            builder.setNegativeButton(neg, negListener);
        }

        return builder.show();
    }
}
