package com.jumbotail.weather.utils;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class GsonUtils {
    private static GsonUtils sInstance;
    private static final String TAG = "GsonUtils";

    private GsonBuilder mGsonBuilder;
    private Gson mGson;

    private GsonUtils(){
        mGsonBuilder = new GsonBuilder();
        mGson = mGsonBuilder.create();
    }

    private static synchronized GsonUtils getInstance(){
        if(sInstance == null){
            sInstance = new GsonUtils();
        }
        return sInstance;
    }

    /**
     * Deserializes the JSON object and returns an object of the the type specified in <i>classPath</i>
     * <p>
     * This method expects the classPath to refer to a non generic type.
     *
     * @param  json     the String representation of incoming JSON object
     * @param  classPath the non generic type of the return object
     * @return           the deserialized object of type classPath if the class exists and json represents an object of the type classPath     * @return           the deserialized object of type classPath if the class exists and json represents an object of the type classPath, null otherwise
     */
    public static <T> T DeserializeJSON(String json, Class classPath) {
        T queryResult = null;

        if (json != null && classPath != null) {
            try {
                queryResult = (T) getInstance().mGson.fromJson(json, classPath);
            } catch (JsonSyntaxException e) {
                Log.e(TAG, "JsonSyntaxException: " + e.toString());
            } catch (JsonIOException e) {
                Log.e(TAG, "JsonIOException " + e.toString());
            }
        }
        return queryResult;
    }

    /**
     * Serializes an object to its JSON representation
     * <p>
     * This method should be used only with non generic objects.
     *
     * @param  object the InputStream to read the incoming JSON object
     * @return        the  JSON representation in form of a String
     */
    public static String SerializeToJson(Object object) {
        return getInstance().mGson.toJson(object);
    }
}
