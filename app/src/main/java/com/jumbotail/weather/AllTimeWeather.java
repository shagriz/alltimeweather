package com.jumbotail.weather;

import android.app.Application;
import android.util.Log;

import com.jumbotail.weather.utils.PicassoInstance;
import com.jumbotail.weather.utils.Utils;

/**
 * Created by shadmananwer on 25/01/16.
 */
public class AllTimeWeather extends Application {

    private static final String TAG = "AllTimeWeather";

    @Override
    public void onCreate() {
        Log.d(TAG, "Starting Application");
        super.onCreate();

        Log.d(TAG, "Setting global appliaction context");
        Utils.SetApplicationContext(getApplicationContext());

        Log.d(TAG, "Initializing picasso instance");
        PicassoInstance.setPicassoSingleton(getApplicationContext());
    }
}
