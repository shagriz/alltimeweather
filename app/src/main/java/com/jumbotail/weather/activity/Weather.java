package com.jumbotail.weather.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.jumbotail.weather.R;
import com.jumbotail.weather.card.DetailsCard;
import com.jumbotail.weather.card.ForecastCard;
import com.jumbotail.weather.card.SummaryCard;
import com.jumbotail.weather.data.Forecast;
import com.jumbotail.weather.data.Observation;
import com.jumbotail.weather.utils.GsonUtils;
import com.jumbotail.weather.utils.LocationUtils;
import com.jumbotail.weather.utils.NetworkUtils;
import com.jumbotail.weather.utils.SharedPreferenceUtils;
import com.jumbotail.weather.utils.Utils;

import java.io.IOException;
import java.util.Date;

public class Weather extends AppCompatActivity implements LocationUtils.LocationCallback {
    private static final String TAG = "Weather";

    private ScrollView mScrollView;
    private Button mGetDataButton;
    private boolean mIsDestroyed;
    private LinearLayout mLinearLayout;

    private SummaryCard mSummaryCard;
    private DetailsCard mDetailsCard;
    private ForecastCard mForecastCard;

    private ProgressDialog mProgressDialog;

    private boolean mDetailsDownloaded;
    private boolean mForecastDownloaded;
    private Location mLocation;
    private AlertDialog mAlertDialog;

    private final long TIME_10_MIN = 1000 * 60 * 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsDestroyed = false;

        setContentView(R.layout.activity_weather);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        mGetDataButton = (Button) findViewById(R.id.get_data_button);
        mScrollView = (ScrollView) findViewById(R.id.scrollView);
        mLinearLayout = (LinearLayout) findViewById(R.id.scroll_linear_layout);

        mGetDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startProcedures();
            }
        });

        mSummaryCard = new SummaryCard(this);
        mDetailsCard = new DetailsCard(this);
        mForecastCard = new ForecastCard(this);

        mLinearLayout.addView(mSummaryCard.createCardView(mLinearLayout, false));
        mLinearLayout.addView(mDetailsCard.createCardView(mLinearLayout, false));
        mLinearLayout.addView(mForecastCard.createCardView(mLinearLayout, false));

        if ( savedInstanceState != null )
            mLocation = savedInstanceState.getParcelable("mLocation");
    }

    @Override
    protected void onResume() {
        super.onResume();

        startProcedures();
    }

    private void startProcedures() {
        DialogInterface.OnClickListener listenerClose = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        };

        if ( !Utils.IsLocationServicesEnabled() ) {
            Log.e(TAG, "Location service not enabled");

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                }
            };

            Utils.ShowAlertDialog(this, "Location not enabled",
                    "Location services are not enabled. Please enable to them",
                    "Enable Data", listener,
                    "Exit App", listenerClose);
            return;
        }

        if ( !NetworkUtils.IsNetworkAvailable() ) {
            Log.e(TAG, "No internet available on device");
            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent myIntent = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
                    startActivity(myIntent);
                }
            };

            Utils.ShowAlertDialog(this, "No Internet",
                    "No internet on device. Please trun on data services",
                    "Enable Data", listener,
                    "Exit App", listenerClose);
            return;
        }

        if ( mLocation == null ) {
            Log.d(TAG, "Checking last location for quick process");
            mLocation = LocationUtils.GetLastKnowLocationCoarse();
        }

        if ( mLocation == null ) {
            Log.w(TAG, "No location data found. Requesting updates");

            if ( mProgressDialog != null )
                mProgressDialog.cancel();
            mProgressDialog = ProgressDialog.show(this, null, "Getting Location");

            LocationUtils.RequestLocationCoarse(this);
            return;
        }

        retrieveData(mLocation);
    }


    private void retrieveData(Location location) {
        mDetailsDownloaded = false;
        mForecastDownloaded = false;

        if ( mProgressDialog != null )
            mProgressDialog.cancel();
        mProgressDialog = ProgressDialog.show(this, null, "Getting Weather");

        String urlWeather = NetworkUtils.GetWeatherURL(location);
        String urlForecast = NetworkUtils.GetForecastURL(location);

        String data_details = SharedPreferenceUtils.getString(SharedPreferenceUtils.KEY_DETAILS_DATA);
        long date_details = SharedPreferenceUtils.getLong(SharedPreferenceUtils.KEY_DETAILS_DATE);

        String data_forecast = SharedPreferenceUtils.getString(SharedPreferenceUtils.KEY_FORECAST_DATA);
        long date_forecast = SharedPreferenceUtils.getLong(SharedPreferenceUtils.KEY_FORECAST_DATE);

        long timeDiff_details = new Date().getTime() - date_details;
        long timeDiff_forecast = new Date().getTime() - date_forecast;
        Log.d(TAG, "Time diff - details - " + (timeDiff_details / (1000 * 60))
                + " Forecast - " + (timeDiff_forecast / (1000 * 60)));

        if ( !Utils.IsEmpty(data_details) && timeDiff_details < TIME_10_MIN ) {
            // Data already there. No need to download new data.
            Log.d(TAG, "Taking form saved data for details");
            Observation observation = GsonUtils.DeserializeJSON(data_details, Observation.class);
            if ( observation == null ) {
                Log.e(TAG, "Observation is null");
                return;
            }

            mSummaryCard.setData(observation.getCurrentObservation());
            mDetailsCard.setData(observation.getCurrentObservation());

            mDetailsDownloaded = true;
            closeDialog();
        } else {
            Log.d(TAG, "Downloading new data for details");
            NetworkUtils.SendAsyncRequest(urlWeather, null, detailsCallback);
        }

        if ( !Utils.IsEmpty(data_forecast) && timeDiff_forecast < TIME_10_MIN ) {
            // Forecast data is already there. No need to download more.
            Log.d(TAG, "Taking form saved data for forecast");
            Forecast forecast = GsonUtils.DeserializeJSON(data_forecast, Forecast.class);
            if ( forecast == null ) {
                Log.e(TAG, "Forecast is null");
                return;
            }

            mForecastCard.setData(forecast.getForecast());

            mForecastDownloaded = true;
            closeDialog();
        } else {
            Log.d(TAG, "Downloading new data for forecast");
            NetworkUtils.SendAsyncRequest(urlForecast, null, forecastCallback);
        }
    }

    private void closeDialog() {
        if ( mDetailsDownloaded && mForecastDownloaded ) {
            if ( mProgressDialog != null )
                mProgressDialog.cancel();

            mScrollView.setVisibility(View.VISIBLE);
            mGetDataButton.setVisibility(View.GONE);
        }
    }

    private void showDownloadErrorMsg() {
        if ( mAlertDialog != null && mAlertDialog.isShowing() ) {
            return;
        }

        DialogInterface.OnClickListener retryListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                startProcedures();
            }
        };

        mAlertDialog = Utils.ShowAlertDialog(this, "Unable to connect",
                "Unable to connect and download data/ Please retry",
                "Retry", retryListener,
                "Cancel", null);
    }

    private NetworkUtils.CompleteCallback detailsCallback = new NetworkUtils.CompleteCallback() {
        @Override
        public void onComplete(NetworkUtils.CompoundResponse response) {
            if ( mIsDestroyed ) {
                Log.e(TAG, "Activity no more");
                return;
            }

            mDetailsDownloaded = true;
            closeDialog();

            if ( response.response.code() != 200 ) {
                Log.e(TAG, "Error retrieving data");
                onFailure(new IOException("Error with response"));
                return;
            }

            Observation observation = GsonUtils.DeserializeJSON(response.body, Observation.class);
            if ( observation == null ) {
                Log.e(TAG, "Observation is null");
                return;
            }

            SharedPreferenceUtils.PutString(SharedPreferenceUtils.KEY_DETAILS_DATA, response.body);
            SharedPreferenceUtils.PutLong(SharedPreferenceUtils.KEY_DETAILS_DATE, new Date().getTime());

            mSummaryCard.setData(observation.getCurrentObservation());
            mDetailsCard.setData(observation.getCurrentObservation());
        }

        @Override
        public void onFailure(IOException exception) {
            if ( mIsDestroyed ) {
                Log.e(TAG, "Activity no more");
                return;
            }

            mDetailsDownloaded = true;
            closeDialog();

            Log.e(TAG, "Exception occurred - " + exception.getMessage());
            exception.printStackTrace();

            showDownloadErrorMsg();
        }
    };

    private NetworkUtils.CompleteCallback forecastCallback = new NetworkUtils.CompleteCallback() {
        @Override
        public void onComplete(NetworkUtils.CompoundResponse response) {
            if ( mIsDestroyed ) {
                Log.e(TAG, "Activity no more");
                return;
            }

            mForecastDownloaded = true;
            closeDialog();

            if ( response.response.code() != 200 ) {
                Log.e(TAG, "Error retrieving data");
                onFailure(new IOException("Error with response"));
                return;
            }

            Forecast forecast = GsonUtils.DeserializeJSON(response.body, Forecast.class);
            if ( forecast == null ) {
                Log.e(TAG, "Forecast is null");
                return;
            }

            SharedPreferenceUtils.PutString(SharedPreferenceUtils.KEY_FORECAST_DATA, response.body);
            SharedPreferenceUtils.PutLong(SharedPreferenceUtils.KEY_FORECAST_DATE, new Date().getTime());

            mForecastCard.setData(forecast.getForecast());
        }

        @Override
        public void onFailure(IOException exception) {
            if ( mIsDestroyed ) {
                Log.e(TAG, "Activity no more");
                return;
            }

            mForecastDownloaded = true;
            closeDialog();

            Log.e(TAG, "Exception occurred - " + exception.getMessage());
            exception.printStackTrace();

            showDownloadErrorMsg();
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_weather, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if ( id == R.id.action_refresh ) {
            startProcedures();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mIsDestroyed = true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("mLocation", mLocation);
    }

    @Override
    public void onLocationReceived(Location location) {
        if ( mIsDestroyed ) {
            Log.e(TAG, "Activity no more");
            return;
        }

        if ( mProgressDialog != null )
            mProgressDialog.cancel();

        retrieveData(location);
    }
}
